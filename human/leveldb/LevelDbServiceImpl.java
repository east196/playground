

package com.github.east196.vizdev.dao;

import java.io.IOException;

import org.springframework.stereotype.Service;

@Service("dbService")
public class LevelDbServiceImpl implements DbService {

	@Override
	public <T> void save(String key, T t) throws IOException {
		
		LevelDbUtil.putObject(key, t);
	}

	@Override
	public <T> T query(String key, Class<T> classes) throws ClassNotFoundException, IOException {
		
		return LevelDbUtil.getObject(key, classes);
	}

	@Override
	public void delete(String key) {
		
		LevelDbUtil.delete(key);
	}


	
}
