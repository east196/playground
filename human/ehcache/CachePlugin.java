package com.github.east196.boot.cache;

import com.github.east196.boot.SpringContextHelper;
import com.github.east196.vizdev.Constants;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class CachePlugin {

    private static CacheManager cacheManager = ((CacheManager)SpringContextHelper.getSpringBean("ehcacheManager"));
    
    public static Cache addCache(String cacheName) {
        cacheManager.addCache(cacheName);
        Cache cache = cacheManager.getCache(cacheName);
        cache.getCacheConfiguration().setEternal(true);
        return cache;
    }
       
    public static void put(String key, Object value) {
        Cache cache = cacheManager.getCache(Constants.CACHE_NAME);
        if (cache != null) {
            Element el = new Element(key, value);
            cache.put(el);
        }
    }

	public static <T> T get(String key) {
        Element element = cacheManager.getCache(Constants.CACHE_NAME).get(key);
        if (element == null) {
            return null;
        }
        return (T)element.getObjectValue();
    }

    public static void putFlowEntity(String flowId, String entityId, Object value) {
    	String key = String.format("%s#%s", flowId, entityId);
    	put(key, value);
    }
    
    public static <T> T getFlowEntity(String flowId, String entityId) {
    	String key = String.format("%s#%s", flowId, entityId);
        return  get(key);
    }
    
    public static void removeCache() {
        cacheManager.removeCache(Constants.CACHE_NAME);
    }

    public static void removeCacheObject(String key) {
        cacheManager.getCache(Constants.CACHE_NAME).remove(key);
    }

    public static void removeCacheObjectAll() {
        cacheManager.getCache(Constants.CACHE_NAME).removeAll();
    }    
}
